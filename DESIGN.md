# Design

Every section can be prepended with "In an questionable move, I have decided to ...".

## Password Storage

WitchAuth does not use the PHC string format.
The password KDF and its parameters are fixed in a version.

Version 1 is scrypt with N=`2 << 15`, r=8 and p=1

```
u64 = 0x01
```
