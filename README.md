# WitchAuth

Small IAM server.

## Please don't mirror to GitHub, GitLab etc.

I can't enforce it of course and I can't stop you.
But please do not mirror this repository to forges that ingest the code for LLMs.

## Why?

WitchAuth is tries to empower small communities and groups to have an identity provider for easy management and better security through SSO.

I believe this can be achieved by chasing two buzzwords:
- **Easy to deploy:** Trivial to run in a container or as a system service (supervised by s6, systemd etc.)
- **Easy to manage:** Uses SQLite to remove database administration work. Stream it with litestream and restart the service when needed.

## Building

The following are required:

- Rust (stable)
- libsodium (>=1.0.19)
- pkgconf or pkg-config
- `sizeof(size_t) == sizeof(uintptr_t)`

## Roadmap (non-ordered)

- [ ] Passable OIDC support with minimum JWT nonsense
    - [ ] OAuth 2.0
    - [ ] OIDC Core
    - [ ] OIDC Discovery

- [ ] At least bare minimum security effort
    - [ ] Somewhat basic login page protection
    - [ ] TOTP
    - [ ] WebAuthn maybe?

- [ ] Smooth Management
    - [ ] Easy to admin via CLI
    - [ ] Easy to admin via API
    - [ ] Easy to admin via a basic panel

- [ ] Alternative storage?
    - [ ] PostgreSQL?
    - [ ] FoundationDB?

- [ ] Documentation
    - [ ] Administrator handbook (antora)
    - [ ] Manpages (scdoc)
    - [ ] Developer documentation (a folder with random files and words)

## Future Work

HSM (yubihsm maybe?) support would be *really* nice.
Hopefully without getting PKCS#11 involved.

SAML? (oh god please no)

## License

```
Copyright (C) 2024 Aydin Mercan <aydin@mercan.dev>

This repository is licensed under the EUPL 1.2.
The English version of the text is included in the LICENSE file.
Please refer to https://joinup.ec.europa.eu/community/eupl/og_page/eupl for more information.
```
