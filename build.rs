fn link_shared_library(name: &str, min_version: &str) {
    let lib = pkg_config::Config::new()
        .atleast_version(min_version)
        .statik(false)
        .probe(name)
        .unwrap();

    for path in &lib.link_paths {
        println!("cargo:lib={}", path.to_str().unwrap());
    }
}

fn main() {
    link_shared_library("libsodium", "1.0.19");
}
