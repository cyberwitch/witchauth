CREATE TABLE IF NOT EXISTS main.users (
    display_id TEXT    NOT NULL UNIQUE,
    username   TEXT    NOT NULL UNIQUE,
    passhash   BLOB    NOT NULL,
    created_at INTEGER NOT NULL
) STRICT;

CREATE TABLE IF NOT EXISTS main.oidc_rs256 (
    keyblob BLOB NOT NULL,
    nonce   BLOB NOT NULL,
    tag     BLOB NOT NULL
) STRICT;
