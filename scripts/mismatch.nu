#!/usr/bin/env nu

let $lock = (open Cargo.lock | from toml | get package | select name version | rename name used)
let $toml = (open Cargo.toml | get dependencies build-dependencies | drop 1 | transpose name info | update info {|row| $row.info.version | str trim --char='=' })

let $diff = ($lock | join $toml name | filter {|x| $x.used != $x.info})

if ($diff | is-empty) { print "no difference found :)" } else { $diff }
