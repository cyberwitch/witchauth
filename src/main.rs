mod keyring;
mod oidc;
mod server;
mod sodium;
mod task;

use std::io::Write;
use std::process::ExitCode;

const HELP_STRING: &[u8] = br#"witchauth - cute iam server

subcommands:
    keygen generate keys
    server start the server
    help display this message and exit

please consult the handbook or the manual page for more information
    p.s. they don't exist at the moment hehe :3
"#;

fn usage() {
    let mut stdout = std::io::stdout().lock();

    stdout.write_all(HELP_STRING).unwrap();
}

fn main() -> ExitCode {
    sodium::init();

    let mut args = std::env::args();

    _ = args.next();

    if let Some(cmd) = args.next() {
        match cmd.as_str() {
            "help" => usage(),
            "keygen" => println!("todo!"),
            "server" => server::start(),
            _ => usage(),
        }
    } else {
        usage();
    }

    ExitCode::SUCCESS
}
