use base64ct::{Base64UrlUnpadded, Encoding};
use bytes::{BufMut, Bytes, BytesMut};
use rand_core::OsRng;
use rsa::pkcs1v15::SigningKey;
use rsa::traits::PublicKeyParts;

use crate::sodium::sha2::Sha256;

pub struct Rs256Signer {
    kid: [u8; 48],
    key: SigningKey<Sha256>,
}

impl Rs256Signer {
    pub fn random() -> Rs256Signer {
        let key = SigningKey::random(&mut OsRng, 2048).unwrap();

        let kid = [0u8; 48];

        Rs256Signer { kid, key }
    }

    pub fn jwk(&self) -> Bytes {
        let mut buf = BytesMut::new();

        buf.put(&br#"{"alg":"RS256","use":"sig","kty":"RSA","e":"AQAB","kid":"#[..]);

        let n = {
            let raw = self.key.as_ref().n().to_be_bytes();
            Base64UrlUnpadded::encode_string(&raw)
        };

        buf.put(&self.kid[..]);
        buf.put(&br#","n":"#[..]);
        buf.put(n.as_bytes());

        buf.freeze()
    }
}
