pub mod rtglue;
pub mod state;

use std::boxed::Box;
use std::future::Future;
use std::net::SocketAddr;
use std::pin::Pin;
use std::rc::Rc;

use http_body_util::Full;
use hyper::body::{Bytes, Incoming};
use hyper::server::conn::http1;
use hyper::{Request, Response};
use matchit::Router;
use tokio::net::TcpListener;

use crate::server::rtglue::TokioGlue;

pub type WitchResult = Result<Response<Full<Bytes>>, hyper::Error>;
pub type WitchFuture = Pin<Box<dyn Future<Output = WitchResult>>>;
pub type WitchFn = Box<dyn Fn(Rc<State>, Request<Incoming>) -> WitchFuture>;

pub trait WitchEndpoint<T> {
    async fn call(self, state: Rc<State>, request: Request<Incoming>) -> WitchResult;
}

pub struct WitchService {
    state: Rc<State>,
    get: Router<WitchFn>,
    post: Router<WitchFn>,
}

pub struct Builder {
    get: Router<WitchFn>,
    post: Router<WitchFn>,
}

pub struct State {
    owo: u32,
}

async fn lost() -> Result<Response<Full<Bytes>>, hyper::Error> {
    Ok(Response::new(Full::new(Bytes::from("uwu ur lost"))))
}

async fn hello() -> Result<Response<Full<Bytes>>, hyper::Error> {
    Ok(Response::new(Full::new(Bytes::from("Hello, World!"))))
}

async fn owo() -> Result<Response<Full<Bytes>>, hyper::Error> {
    Ok(Response::new(Full::new(Bytes::from("owo!"))))
}

async fn index(s: Rc<State>) -> Result<Response<Full<Bytes>>, hyper::Error> {
    Ok(Response::new(Full::new(Bytes::from(format!(
        "index!, {}",
        s.owo
    )))))
}

impl<F, Fut> WitchEndpoint<()> for F
where
    F: FnOnce() -> Fut,
    Fut: Future<Output = WitchResult>,
{
    async fn call(self, _: Rc<State>, _: Request<Incoming>) -> WitchResult {
        self().await
    }
}

impl<F, Fut> WitchEndpoint<(Rc<State>,)> for F
where
    F: FnOnce(Rc<State>) -> Fut,
    Fut: Future<Output = WitchResult>,
{
    async fn call(self, state: Rc<State>, _: Request<Incoming>) -> WitchResult {
        self(state).await
    }
}

impl<F, Fut> WitchEndpoint<(Rc<State>, Request<Incoming>)> for F
where
    F: FnOnce(Rc<State>, Request<Incoming>) -> Fut,
    Fut: Future<Output = WitchResult>,
{
    async fn call(self, state: Rc<State>, request: Request<Incoming>) -> WitchResult {
        self(state, request).await
    }
}

impl Builder {
    pub fn new() -> Builder {
        Builder {
            get: Router::new(),
            post: Router::new(),
        }
    }

    pub fn get<T, E>(mut self, path: &str, endpoint: E) -> Builder
    where
        T: 'static,
        E: WitchEndpoint<T> + Copy + 'static,
    {
        self.get
            .insert(path, Box::new(move |s, r| Box::pin(endpoint.call(s, r))))
            .unwrap();

        self
    }

    pub fn post<T, E>(mut self, path: &str, endpoint: E) -> Builder
    where
        T: 'static,
        E: WitchEndpoint<T> + Copy + 'static,
    {
        self.post
            .insert(path, Box::new(move |s, r| Box::pin(endpoint.call(s, r))))
            .unwrap();

        self
    }

    pub fn build(self, state: State) -> WitchService {
        WitchService {
            state: Rc::new(state),
            get: self.get,
            post: self.post,
        }
    }
}

impl hyper::service::Service<Request<Incoming>> for WitchService {
    type Response = Response<Full<Bytes>>;
    type Error = hyper::Error;
    type Future = WitchFuture;

    fn call(&self, req: Request<Incoming>) -> Self::Future {
        let s = Rc::clone(&self.state);

        match self.get.at(req.uri().path()) {
            Ok(route) => (*route.value)(s, req),
            Err(_) => Box::pin(lost()),
        }
    }
}

async fn doit() {
    let srv = Rc::new(
        Builder::new()
            .get("/", index)
            .get("/owo", owo)
            .post("/hello", hello)
            .build(State { owo: 3 }),
    );

    println!("Starting!");

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));

    // We create a TcpListener and bind it to 127.0.0.1:3000
    let listener = TcpListener::bind(addr).await.unwrap();

    loop {
        let (s, _) = listener.accept().await.unwrap();

        let io = TokioGlue::new(s);

        let s = Rc::clone(&srv);

        tokio::task::spawn_local(async move {
            if let Err(err) = http1::Builder::new().serve_connection(io, s).await {
                eprintln!("Error serving connection: {:?}", err);
            }
        });
    }
}

pub fn start() {
    let rt = tokio::runtime::Builder::new_current_thread()
        .enable_io()
        .build()
        .unwrap();

    let local = tokio::task::LocalSet::new();

    local.spawn_local(doit());

    rt.block_on(local);
}
