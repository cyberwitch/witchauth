use tokio::sync::oneshot;

use crate::task::{TaskInjector, WitchTask};

pub struct State {
    task_q: TaskInjector,
}

impl State {
    pub fn new(task_q: TaskInjector) -> State {
        State { task_q }
    }

    pub async fn enqueue<T, R>(&self, task: T) -> Result<R, ()>
    where
        T: WitchTask<Return = R> + 'static,
        R: Send + 'static,
    {
        let (tx, rx) = oneshot::channel();

        let func = Box::new(move || {
            let ret = task.call();

            let _ = tx.send(ret);
        });

        self.task_q.push(func);

        match rx.await {
            Ok(r) => r,
            Err(_) => Err(()),
        }
    }
}
