pub mod hkdf;
pub mod sha2;
pub mod sys;

/// Initialize libsodium for usage.
/// Will abort process on failure.
///
/// <div class="warning">
///     Failing to call this function before using any of the functions is unsafe.
/// </div>
pub fn init() {
    unsafe {
        if sys::sodium_init() < 0 {
            std::process::abort();
        }
    }
}
