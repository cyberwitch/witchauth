use crate::sodium::sys::crypto_pwhash_argon2id;

pub fn argon2id<const L: usize>(pass: &[u8], salt: &[u8]) -> [u8; L] {}
