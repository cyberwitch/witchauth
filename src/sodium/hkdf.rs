use crate::sodium::sys;

#[repr(transparent)]
pub struct Hkdf_Sha2_256([u8; 32]);

impl Hkdf_Sha2_256 {
    pub fn new(ikm: &[u8], salt: &[u8]) -> Hkdf_Sha2_256 {
        let mut prk = [0u8; 32];

        unsafe {
            sys::crypto_kdf_hkdf_sha256_extract(
                prk.as_mut_slice().as_mut_ptr(),
                salt.as_ptr(),
                salt.len(),
                ikm.as_ptr(),
                ikm.len(),
            );
        }

        Hkdf_Sha2_256(prk)
    }

    pub fn derive<const L: usize>(&self, info: &[u8]) -> [u8; L] {
        let mut key = [0u8; L];

        unsafe {
            sys::crypto_kdf_hkdf_sha256_expand(
                key.as_mut_slice().as_mut_ptr(),
                L,
                info.as_ptr() as *const _,
                info.len(),
                self.0.as_ptr(),
            );
        }

        key
    }
}

#[cfg(test)]
mod tests {
    use crate::sodium::sys::sodium_init;

    use crate::sodium::hkdf::*;

    #[test]
    fn sha2_256() {
        unsafe {
            assert!(sodium_init() >= 0);
        }

        let ikm = [0x0b; 22];
        let info = [0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9];
        let salt = [
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c,
        ];

        let kdf = Hkdf_Sha2_256::new(&ikm, &salt);

        let actual = kdf.derive(&info);

        let expected = [
            0x3c, 0xb2, 0x5f, 0x25, 0xfa, 0xac, 0xd5, 0x7a, 0x90, 0x43, 0x4f, 0x64, 0xd0, 0x36,
            0x2f, 0x2a, 0x2d, 0x2d, 0x0a, 0x90, 0xcf, 0x1a, 0x5a, 0x4c, 0x5d, 0xb0, 0x2d, 0x56,
            0xec, 0xc4, 0xc5, 0xbf, 0x34, 0x00, 0x72, 0x08, 0xd5, 0xb8, 0x87, 0x18, 0x58, 0x65,
        ];

        assert_eq!(expected, actual);
    }
}
