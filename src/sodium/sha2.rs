use std::convert::AsRef;
use std::mem::MaybeUninit;

use digest::const_oid::{AssociatedOid, ObjectIdentifier};
use digest::{Digest, Output, OutputSizeUser};
use rsa::pkcs1v15::RsaSignatureAssociatedOid;

use crate::sodium::sys;

/// SHA-2-256 hash function backed by libsodium.
/// Implements the [`digest::Digest`] trait.
#[repr(transparent)]
pub struct Sha256(sys::crypto_hash_sha256_state);

impl OutputSizeUser for Sha256 {
    type OutputSize = digest::typenum::U32;
}

impl RsaSignatureAssociatedOid for Sha256 {
    const OID: ObjectIdentifier = ObjectIdentifier::new_unwrap("1.2.840.113549.1.1.11");
}

impl AssociatedOid for Sha256 {
    const OID: ObjectIdentifier = ObjectIdentifier::new_unwrap("2.16.840.1.101.3.4.2.1");
}

impl Digest for Sha256 {
    fn new() -> Self {
        let mut state = MaybeUninit::<sys::crypto_hash_sha256_state>::uninit();

        unsafe {
            sys::crypto_hash_sha256_init(state.as_mut_ptr());

            Self(state.assume_init())
        }
    }

    fn new_with_prefix(data: impl AsRef<[u8]>) -> Self {
        let mut s = Sha256::new();
        s.update(data);
        s
    }

    fn update(&mut self, data: impl AsRef<[u8]>) {
        unsafe {
            sys::crypto_hash_sha256_update(
                &mut self.0,
                data.as_ref().as_ptr(),
                data.as_ref().len() as _,
            );
        }
    }

    fn chain_update(mut self, data: impl AsRef<[u8]>) -> Self {
        self.update(data);
        self
    }

    fn finalize(mut self) -> Output<Self> {
        let mut out = [0u8; 32];

        unsafe {
            sys::crypto_hash_sha256_final(&mut self.0, out.as_mut_ptr());
        }

        out.into()
    }

    fn finalize_into(mut self, out: &mut Output<Self>) {
        unsafe {
            sys::crypto_hash_sha256_final(&mut self.0, out.as_mut_ptr());
        }
    }

    fn finalize_reset(&mut self) -> Output<Self> {
        let mut out = [0u8; 32];

        unsafe {
            sys::crypto_hash_sha256_final(&mut self.0, out.as_mut_ptr());
            sys::crypto_hash_sha256_init(&mut self.0);
        }

        out.into()
    }

    fn finalize_into_reset(&mut self, out: &mut Output<Self>) {
        unsafe {
            sys::crypto_hash_sha256_final(&mut self.0, out.as_mut_ptr());
            sys::crypto_hash_sha256_init(&mut self.0);
        }
    }

    fn reset(&mut self) {
        unsafe {
            sys::crypto_hash_sha256_init(&mut self.0);
        }
    }

    fn output_size() -> usize {
        32
    }

    fn digest(data: impl AsRef<[u8]>) -> Output<Self> {
        Self::new().chain_update(data).finalize()
    }
}
