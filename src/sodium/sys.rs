//! Raw bindings to libsodium. Requires AES acceleration.
//!
//! Only used functions are provided.
//! Because the ABI is stable, structures are also declared.

use std::ffi::{c_char, c_int, c_uchar, c_ulonglong};

pub const AEGIS256_KEY_SIZE: usize = 32;
pub const AEGIS256_MAC_SIZE: usize = 32;
pub const AEGIS256_NONCE_SIZE: usize = 32;

#[repr(C)]
pub struct crypto_hash_sha256_state {
    state: [u32; 8],
    count: u64,
    buf: [u8; 64],
}

extern "C" {
    pub fn sodium_init() -> c_int;

    pub fn crypto_aead_aegis256_encrypt_detached(
        c: *mut c_uchar,
        mac: *mut c_uchar,
        maclen_p: *mut c_ulonglong,
        m: *const c_uchar,
        mlen: c_ulonglong,
        ad: *const c_uchar,
        adlen: c_ulonglong,
        nsec: *const c_uchar,
        npub: *const c_uchar,
        k: *const c_uchar,
    ) -> c_int;

    pub fn crypto_aead_aegis256_decrypt_detached(
        m: *mut c_uchar,
        nsec: *mut c_uchar,
        c: *const c_uchar,
        clen: c_ulonglong,
        mac: *const c_uchar,
        ad: *const c_uchar,
        adlen: c_ulonglong,
        npub: *const c_uchar,
        k: *const c_uchar,
    ) -> c_int;

    pub fn crypto_hash_sha256_init(state: *mut crypto_hash_sha256_state) -> c_int;

    pub fn crypto_hash_sha256_update(
        state: *mut crypto_hash_sha256_state,
        input: *const c_uchar,
        inlen: c_ulonglong,
    ) -> c_int;

    pub fn crypto_hash_sha256_final(
        state: *mut crypto_hash_sha256_state,
        out: *mut c_uchar,
    ) -> c_int;

    pub fn crypto_kdf_hkdf_sha256_expand(
        out: *mut c_uchar,
        out_len: usize,
        ctx: *const c_char,
        ctx_len: usize,
        prk: *const c_uchar,
    ) -> c_int;

    pub fn crypto_kdf_hkdf_sha256_extract(
        prk: *mut c_uchar,
        salt: *const c_uchar,
        salt_len: usize,
        ikm: *const c_uchar,
        ikm_len: usize,
    ) -> c_int;

    pub fn crypto_pwhash_argon2id(
        out: *mut c_uchar,
        outlen: c_ulonglong,
        passwd: *const c_char,
        passwdlen: c_ulonglong,
        salt: *const c_uchar,
        opslimit: c_ulonglong,
        memlimit: usize,
        alg: c_int,
    ) -> c_int;
}

#[cfg(test)]
mod tests {
    use crate::sodium::sys::*;

    extern "C" {
        fn crypto_aead_aegis256_abytes() -> usize;
        fn crypto_aead_aegis256_keybytes() -> usize;
        fn crypto_aead_aegis256_npubbytes() -> usize;
    }

    #[test]
    fn constants_correct() {
        unsafe {
            assert!(sodium_init() >= 0);

            assert_eq!(crypto_aead_aegis256_abytes(), AEGIS256_MAC_SIZE);
            assert_eq!(crypto_aead_aegis256_keybytes(), AEGIS256_KEY_SIZE);
            assert_eq!(crypto_aead_aegis256_npubbytes(), AEGIS256_NONCE_SIZE);
        }
    }
}
