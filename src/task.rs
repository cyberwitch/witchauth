use std::cell::UnsafeCell;
use std::sync::mpsc;
use std::sync::Arc;
use std::thread;
use std::vec::Vec;

use crossbeam_deque::{Injector, Stealer, Worker};

pub type TaskFn = dyn FnOnce() + Send + 'static;

pub type TaskInjector = Injector<Box<TaskFn>>;

pub trait WitchTask: Send {
    type Return;

    fn call(self) -> Result<Self::Return, ()>;
}

pub trait ReadWriteTask: Send {
    type Return;

    fn call(self) -> Result<Self::Return, ()>;
}

pub trait ReadOnlyTask: Send {
    type Return;

    fn call(self) -> Result<Self::Return, ()>;
}

pub struct TaskPool {
    handles: Box<[thread::JoinHandle<()>]>,
    steal: Arc<Box<[Stealer<Box<TaskFn>>]>>,
}

pub struct TaskScheduler {
    rw_tx: mpsc::SyncSender<Box<TaskFn>>,
    ro_injector: Injector<Box<TaskFn>>,
}

fn rw_loop(rx: mpsc::Receiver<Box<TaskFn>>) {}

fn ro_loop(local_q: Worker<Box<TaskFn>>, steal: Arc<Box<[Stealer<Box<TaskFn>>]>>) {}

impl TaskPool {
    pub fn new(size: usize) -> (TaskPool, TaskInjector) {
        let mut handles = Vec::with_capacity(size);
        let mut workers = Vec::with_capacity(size);
        let mut stealers = Vec::with_capacity(size);

        let injector = Injector::new();

        for i in 0..size {
            let w = Worker::new_fifo();
            let s = w.stealer();

            workers.push(w);
            stealers.push(s);
        }

        let steal = Arc::new(stealers.into_boxed_slice());

        for w in workers {
            let s = Arc::clone(&steal);
            std::thread::spawn(move || ro_loop(w, s));
        }

        let pool = TaskPool {
            handles: handles.into_boxed_slice(),
            steal,
        };

        (pool, injector)
    }

    pub fn wait(self) {
        for h in self.handles {
            _ = h.join();
        }
    }
}
